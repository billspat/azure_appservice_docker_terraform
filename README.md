# Django starter app for Azure App Service with Custom Linux Container and Terraform deployment

**WORK IN PROGRESS**

There are tutorials and example apps for 1) running basic 
Python app using cutom container and 2) deploying an app service with known container using terraform, but not many that combine all of these elements.  This code aims to do that.    

This is based on a very simple Python Django application running in a Docker container. The custom image uses port 8000. 

## Setting up custom image for web App on Linux 
- Create a Web App on Linux using CLI or Azure portal
- Configure your web app to custom image 
- Add an App Setting ```WEBSITES_PORT = 8000 ``` for your app 
- Browse your site 
 
# Contributing

This project is based on the Microsoft tutorial https://github.com/Azure-Samples/docker-django-webapp-linux which is MIT Licensed.    See that URL for the original code, which was modified in several places here.   

