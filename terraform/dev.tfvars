#### project tfvars file

# Project-level
###############
project     = "tftestproj"
prefix      = "test"
environment = "test"
location    = "centralus"
user       =  "billspat"

# Azure
#######
azure-subscription-id = "aa32e19c-49b5-478c-af56-fc710b1a8a1c"

# for use with a service principle
# azure-client-id       = ""
# azure-client-secret   = ""

# Container
container_registry_name = "tftestproj"  # use locals instead project + acr

# App Service

appservice_docker_image = "tftestproj"

