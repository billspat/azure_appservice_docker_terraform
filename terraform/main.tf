# test3/main.tf
# azure app service, storage for the logs, and keyvault. 
# totally stolen/adapted from https://stackoverflow.com/questions/69571719/terraform-app-service-wont-connect-to-storage-account
# Pat Bills May 2022

terraform {
  required_version = ">=1.1"

  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>3.7"
    }
  }

}


############
# providers
provider "azurerm" {
  features {}
}

provider "random"{}

provider "time" {}

resource "random_string" "myrandom" {
  length  = 6
  number  = false
  upper   = false
  special = false
}

data "azurerm_client_config" "current"{}

data "azurerm_resource_group" "main"{
    name="ads-billspat-testing-rg"
}

locals {
  # Common tags to be assigned to all resources
  # id number can be used to select and delete resources
  common_tags = {
    created_by = "billspat"
    project   = "profdev"
    environment = "training"
    created_on = timestamp()
    id = random_string.myrandom.result
  }

  website_name = "${var.website_name_base}${random_string.myrandom.id}"
}

// This gets the Azure AD Tenant ID information to deploy for KeyVault. 
resource "azurerm_key_vault" "key_vault" {
  name                       = local.website_name
  resource_group_name        = data.azurerm_resource_group.main.name
  location                   = data.azurerm_resource_group.main.location
  sku_name                   = "standard"
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days = 7

  tags = "${local.common_tags}"

}

resource "azurerm_key_vault_access_policy" "client" { // This is for AD Users Logged into Azure to give them the right access when creating resources. 
  key_vault_id        = azurerm_key_vault.key_vault.id
  tenant_id           = data.azurerm_client_config.current.tenant_id
  object_id           = data.azurerm_client_config.current.object_id
  secret_permissions  = ["Backup", "Delete", "Get", "List", "Purge", "Recover", "Restore", "Set", ]
  key_permissions     = ["Backup", "Create", "Decrypt", "Delete", "Encrypt", "Get", "Import", "List", "Purge", "Recover", "Restore", "Sign", "UnwrapKey", "Update", "Verify", "WrapKey", ]
  storage_permissions = ["Backup", "Delete", "DeleteSAS", "Get", "GetSAS", "List", "ListSAS", "Purge", "Recover", "RegenerateKey", "Restore", "Set", "SetSAS", "Update", ]
}






