
#### app service : service plan, app service, custom container 

### variables used
# "appservice_docker_image" 
# "appservice_docker_tag" { 

locals {
    # these settings are for the default case where Docker file and build context is the parent directory
    # if that's not true for yoru setup, edit here, or use variables instead 
    dockerfile = "../Dockerfile"
    dockerbuildfolder = ".."
    appservice_docker_image_spec  = "${var.container_registry_name}/${var.appservice_docker_image}:${var.appservice_docker_tag}"

}


################
# appservice custom container - build process
# note this all assumes the Dockerfile and build context 
resource "null_resource" "acr_build" {

  # attempt to re-run this command when the dockerfile changes content (and hence sha1 changes)
  # idea from https://github.com/hashicorp/terraform/issues/11418
  triggers = {
    dockerfile_update = "${sha1(file(local.dockerfile))}"
  } 

  provisioner "local-exec" {
    # command assumed Docker context is parent directory
    command = "az acr build -t ${var.appservice_docker_image}:${var.appservice_docker_tag} -r ${var.container_registry_name} --file ${local.dockerfile}  ${local.dockerbuildfolder}"
  }
} 


################
# storage accounts for app service

resource "azurerm_storage_account" "website_log_storage" {
  name                     = "${local.website_name}storage"
  resource_group_name      = data.azurerm_resource_group.main.name
  location                 = data.azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  identity {
    type = "SystemAssigned"
  }

  tags = "${local.common_tags}"

}

resource "azurerm_storage_container" "website_logs_container" {
  name                  = "${local.website_name}containerlogs"
  storage_account_name  = azurerm_storage_account.website_log_storage.name
}

resource "time_rotating" "main" {
  rotation_rfc3339 = null
  rotation_years   = 2

  triggers = {
    end_date = null
    years    = 2
  }
}

data "azurerm_storage_account_blob_container_sas" "website_logs_container_sas" {
  connection_string = azurerm_storage_account.website_log_storage.primary_connection_string
  container_name    = azurerm_storage_container.website_logs_container.name


  start  = timestamp()
  expiry = time_rotating.main.rotation_rfc3339

  permissions {
    read   = true
    add    = true
    create = true
    write  = true
    delete = true
    list   = true
  }

  cache_control       = "max-age=5"
  content_disposition = "inline"
  content_encoding    = "deflate"
  content_language    = "en-US"
  content_type        = "application/json"
}

resource "azurerm_storage_account" "website_installers_account" {
  name                     = "nscwebstoredinstallersac"
  resource_group_name      = data.azurerm_resource_group.main.name
  location                 = data.azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  identity {
    type = "SystemAssigned"
  }

  tags = "${local.common_tags}"
}


resource "azurerm_storage_container" "website_installers_container" {

  name                  = "${local.website_name}install"
  storage_account_name  = azurerm_storage_account.website_installers_account.name
  container_access_type = "private"

}

/*
## This Should be used for Windows App Service instead of container
resource "azurerm_storage_share" "website_installers_share" {
  name                 = "${local.website_name}-installersfileshare"
  storage_account_name = azurerm_storage_account.website_installers_account.name
  quota                = 50
}
*/

############
# secrets for app service to access storage accounts

resource "azurerm_key_vault_access_policy" "website_logs_storage_accesspolicy" { // This is for the Storage Account for Website Logs. 
  key_vault_id       = azurerm_key_vault.key_vault.id
  tenant_id          = data.azurerm_client_config.current.tenant_id
  object_id          = azurerm_storage_account.website_log_storage.identity[0].principal_id
  key_permissions    = ["Get", "Create", "Delete", "List", "Restore", "Recover", "UnwrapKey", "WrapKey", "Purge", "Encrypt", "Decrypt", "Sign", "Verify", ]
  secret_permissions = ["Backup", "Delete", "Get", "List", "Purge", "Recover", "Restore", "Set", ]
}

resource "azurerm_key_vault_key" "website_logs_key" {
  name         = "website-logs-key"
  key_vault_id = azurerm_key_vault.key_vault.id

  key_type = "RSA"
  key_size = 2048
  key_opts = ["decrypt", "encrypt", "sign", "unwrapKey", "verify", "wrapKey", ]

  depends_on = [
    azurerm_key_vault_access_policy.client,
    azurerm_key_vault_access_policy.website_logs_storage_accesspolicy
  ]

}

################
# service plan and service

resource "azurerm_app_service_plan" "websiteappserviceplan" {
  name                = "${local.website_name}-plan"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  kind = "Linux" 
  reserved = true

  sku {
    tier = "Standard"
    size = "B1"
  }

  tags = "${local.common_tags}"
}


resource "azurerm_app_service" "app_service" {
  name                = "${local.website_name}-app"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  app_service_plan_id = azurerm_app_service_plan.websiteappserviceplan.id

  app_settings = {
    "KEY_VAULT_URL"                        = azurerm_key_vault.key_vault.vault_uri
    # Settings for private Container Registires  
    DOCKER_REGISTRY_SERVER_URL      = "https://${azurerm_container_registry.acr.login_server}"
    DOCKER_REGISTRY_SERVER_USERNAME = azurerm_container_registry.acr.admin_username
    DOCKER_REGISTRY_SERVER_PASSWORD = azurerm_container_registry.acr.admin_password

  }

  site_config {
    always_on = true
    app_command_line = ""
    linux_fx_version = "DOCKER|${local.appservice_docker_image_spec}"
  }

  # this is for linux app
    storage_account {
    name         = "WebsiteStorageConnectionString"
    type         = "AzureBlob"
    account_name = azurerm_storage_account.website_installers_account.name
    access_key   = azurerm_storage_account.website_installers_account.primary_access_key
    share_name   = azurerm_storage_container.website_installers_container.name
    mount_path   = "    " 
  }
  

  logs {
    detailed_error_messages_enabled = true
    failed_request_tracing_enabled = true

    application_logs {
      azure_blob_storage {
        level="Information"
        sas_url = format("https://${azurerm_storage_account.website_log_storage.name}.blob.core.windows.net/${azurerm_storage_container.website_logs_container.name}%s", data.azurerm_storage_account_blob_container_sas.website_logs_container_sas.sas)
        retention_in_days = 365
      }
    }

    http_logs {
      azure_blob_storage{
        sas_url=format("https://${azurerm_storage_account.website_log_storage.name}.blob.core.windows.net/${azurerm_storage_container.website_logs_container.name}%s", data.azurerm_storage_account_blob_container_sas.website_logs_container_sas.sas)
        retention_in_days = 365
      }
    }
  }

  connection_string {
    name  = "StorageAccount"
    type  = "Custom"
    value = azurerm_storage_account.website_log_storage.primary_connection_string
  }

  identity {
    type = "SystemAssigned"
  }

  tags = "${local.common_tags}"

}

################
# access to storage accounts from app service

resource "azurerm_key_vault_access_policy" "website_accesspolicy" {
  key_vault_id       = azurerm_key_vault.key_vault.id
  tenant_id          = azurerm_app_service.app_service.identity[0].tenant_id
  object_id          = azurerm_app_service.app_service.identity[0].principal_id
  secret_permissions = ["Get"]
}

# output "app_id" {
#     value = azurerm_app_service.example.identity.0.principal_id
#     description = "system assigned identity"
# }