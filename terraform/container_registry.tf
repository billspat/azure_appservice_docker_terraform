# Azure Container Regristry

variable "container_registry_name" { 
  type        = string
  description = "name of the registry to create"
  default = "appsvcsample"
}


resource "azurerm_container_registry" "acr" {
  name                     = var.container_registry_name
  resource_group_name      = azurerm_resource_group.acr-rg.name
  location                 = azurerm_resource_group.acr-rg.location
  sku                      = "Standard"
  # this is slightly less secure as it allows the info to be shared with the app service
  admin_enabled            = true

  # the other option taken is to use system identity 
  tags = "${local.common_tags}"
}

# # means to push up new container image when it's created
# # non functioning WIP
#
# resource "null_resource" "docker_push" {
#     provisioner "local-exec" {
#     command = <<-EOT
#     docker login ${azurerm_container_registry.acr.login_server} 
#     docker push ${azurerm_container_registry.acr.login_server}
#     EOT
#     }
# }


