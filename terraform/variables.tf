### Azure standard

variable "location" {
  type = string
  description = ""
}

variable "azure-subscription-id" {
  type = string
  description = ""
}

### meta data for tags, locals
variable "common_id" {
  type = string
  description = "common id value to use when naming resources for each "
  default = "billspat"
}

variable "user" {
    type = string
    description = "user id/name of author of resources for tagging"
}

### main project/site name
variable "website_name_base" {
  type = string
  description = "base name of website, to be appended with prefix/suffix"
}

variable "project" {
  type = string
  description = ""
}

variable "prefix" {
  type = string
  description = ""
}

variable "environment" {
  type = string
  description = ""
}

### App Service & Container name

variable "appservice_docker_image" { 
  type        = string
  description = "full image spec of docker image to pull"
  default = "basic-django-app"
  
}

variable "appservice_docker_tag" { 
  type        = string
  description = "docker image tag"
  default = "latest"
}
